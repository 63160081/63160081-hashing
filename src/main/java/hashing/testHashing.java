
package hashing;

public class testHashing {
    public static void main(String[] args) {
        Hashing<String, Integer> hash = new Hashing<>();
        hash.add("test",2);
        hash.add("hash",6);
        hash.add("key",4);
        hash.add("hello",5);
        System.out.println(hash.size());
        System.out.println(hash.remove("hello"));
        System.out.println(hash.size());
        System.out.println(hash.isEmpty());
    }
}
