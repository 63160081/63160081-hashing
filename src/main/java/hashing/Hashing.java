package hashing;

// Create class for node.
import java.util.ArrayList;
import java.util.Objects;

class HashNode<K, V> {

    K key;
    V value;

    final int hashCode;

    //Continue node
    HashNode<K, V> next;

    // Constructor.
    public HashNode(K key, V value, int hashCode) {
        this.key = key;
        this.value = value;
        this.hashCode = hashCode;
    }

}

// Class for finding Hash.
public class Hashing<K, V> {

    //Create array for keep Hash.
    private ArrayList<HashNode<K, V>> tableArray;
    //Count of array.
    private int num;
    //Size of array.
    private int size;

    //create class for kepp value
    public Hashing() {
        tableArray = new ArrayList<>();
        num = 10;
        size = 0;

        //Add empty field
        for (int i = 0; i < num; i++)
        {
            tableArray.add(null);
        }
    }

    //getting array size.
    public int size() {
        return size;
    }

    //Checking if size is empty.
    public boolean isEmpty() {
        return size() == 0;
    }

    //Getting Hashcode.
    private final int hashCode(K key) {
        return Objects.hashCode(key);
    }

    //geting index in table.
    private int getTableIndex(K key) {
        int hashCode = hashCode(key);
        int index = hashCode % num;

        index = index < 0 ? index * -1 : index;
        return index;
    }
    
    //removing key 
    public V remove(K key) {

        //getting key index.
        int bucketIndex = getTableIndex(key);
        int hashCode = hashCode(key);

        HashNode<K, V> head = tableArray.get(bucketIndex);

        //getting key
        HashNode<K, V> prev = null;
        while (head != null)
        {

            // cheking key if not null
            if (head.key.equals(key) && hashCode == head.hashCode)
            {
                break;
            }

            prev = head;
            head = head.next;
        }

        // cheking key if null
        if (head == null)
        {
            return null;
        }

        //reduce size.
        size--;

        // remove key
        if (prev != null)
        {
            prev.next = head.next;
        } else
        {
            tableArray.set(bucketIndex, head.next);
        }
        return head.value;
    }
    
    //Class getting key
    public V get(K key)
    {
    	//Getting key index
        int tableIndex = getTableIndex(key);
        int hashCode = hashCode(key);
       
        HashNode<K, V> head = tableArray.get(tableIndex);
        
        // getting key.
        while (head != null) {
            if (head.key.equals(key) && head.hashCode == hashCode)
                return head.value;
            head = head.next;
        }
 
        // if key is null
        return null;
    }
    
    //adding key
    public void add(K key, V value)
    {
        // getting keyu index.
        int tableIndex = getTableIndex(key);
        int hashCode = hashCode(key);
        HashNode<K, V> head = tableArray.get(tableIndex);
 
        // Checkking null in index
        while (head != null) {
            if (head.key.equals(key) && head.hashCode == hashCode) {
                head.value = value;
                return;
            }
            head = head.next;
        }
 
        // fiding key.
        size++;
        head = tableArray.get(tableIndex);
        HashNode<K, V> newNode = new HashNode<K, V>(key, value, hashCode);
        newNode.next = head;
        tableArray.set(tableIndex, newNode);
 
        if ((1.0 * size) / num >= 0.7) {
            ArrayList<HashNode<K, V> > temp = tableArray;
            tableArray = new ArrayList<>();
            num = 2 * num;
            size = 0;
            for (int i = 0; i < num; i++)
                tableArray.add(null);
 
            for (HashNode<K, V> headNode : temp) {
                while (headNode != null) {
                    add(headNode.key, headNode.value);
                    headNode = headNode.next;
                }
            }
        }
    }



}
